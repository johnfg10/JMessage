package io.github.johnfg10

import io.github.johnfg10.providers.GoogleAuthProvider
import io.github.johnfg10.stores.IDPUserStore
import io.github.johnfg10.stores.ProfileStore
import io.github.johnfg10.stores.TokenStore
import io.github.johnfg10.stores.UserStore
import io.vertx.core.AbstractVerticle
import io.vertx.core.Context
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.http.HttpServer
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.ext.jwt.JWK
import io.vertx.ext.jwt.JWT
import io.vertx.ext.jwt.JWTOptions
import io.vertx.ext.web.Router

/**
 * provides id checking and other useful stuff to the cluster
 */
class AuthVerticle : AbstractVerticle() {
    //todo add config options
    val idpUserStore: IDPUserStore = IDPUserStore.Create(vertx)
    val userStore: UserStore = UserStore.Create(vertx)
    val tokenStore: TokenStore = TokenStore.Create(vertx)
    val profileStore: ProfileStore = ProfileStore.Create(vertx)
    //todo read this from a config
    val authHandlers: MutableList<AuthHandlerProvider> = mutableListOf(GoogleAuthProvider("", "", vertx, router))
    //todo read this from a config
    private val hmac = "testkey"

    lateinit var httpServer: HttpServer
    lateinit var router: Router

    override fun init(vertx: Vertx, context: Context) {
        httpServer = vertx.createHttpServer()
        router = Router.router(vertx)
        super.init(vertx, context)
    }

    override fun start() {
        //returns either a error or a User object
        vertx.eventBus().consumer<JsonObject>("io.github.johnfg10.auth.user"){
            val id = it.body().getLong("id")
            userStore.GetUserId(id){res ->
                run {
                    if (res.succeeded()) {
                        //reply with a json encoding of the User object
                        it.reply(Json.encode(res.result()))
                    } else {
                        it.fail(1, res.cause().message)
                    }
                }
            }
        }
        //html
        router.get("/login").produces("text/html").handler {
            //login user using which ever method they chose.
            //then get a id
            // then assign that id to the user which we can validate

            JWT().addJWK(JWK("HS512", hmac)).sign(JsonObject.mapFrom(JWTUserInfo(1)), JWTOptions().setAlgorithm("HS512"))
        }
        //default
        router.get("/login").produces("application/json").handler {
            var authProvidersButtons = ""
            authHandlers.forEach { authProvidersButtons += it.GetAuthProviderButton() + "<br>" }

            it.response().end("""
                <!doctype html>
                <html lang="en">
                <head>
                  <meta charset="utf-8">
                  <title>Login</title>
                </head>
                <body>
                """ +
                    (authHandlers.forEach { it.GetAuthProviderButton() })+
                """
                </body>
                </html>
            """.trimIndent())
        }

        authHandlers.forEach {
            router.get("/login/" + it.GetAuthName()).handler(it::HandleAuth).handler {
                println(it.bodyAsJson.encodePrettily())
            }
        }

        httpServer.requestHandler(router::accept).listen(8080)

        super.start()
    }

    override fun stop() {
        super.stop()
    }
}