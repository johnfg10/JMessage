package io.github.johnfg10.stores

import io.github.johnfg10.stores.impl.ProfileStoreImpl
import io.github.johnfg10.stores.impl.TokenStoreImpl
import io.github.johnfg10.stores.models.Profile
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

interface ProfileStore {
    fun InsertProfile(profile: Profile, handler: (AsyncResult<Int>) -> Unit)

    fun DeleteProfile(profile: Profile, handler: (AsyncResult<Int>) -> Unit)

    fun UpdateProfile(profile: Profile, handler: (AsyncResult<Int>) -> Unit)

    fun GetProfileInternalId(id: Long, handler: (AsyncResult<Profile>) -> Unit)

    fun GetProfileUserId(id: Long, handler: (AsyncResult<Profile>) -> Unit)

    companion object {
        fun Create(vertx: Vertx) : ProfileStore{
            return ProfileStoreImpl(vertx)
        }

        fun Create(vertx: Vertx, config: JsonObject) : ProfileStore{
            return ProfileStoreImpl(vertx, config)
        }
    }
}