package io.github.johnfg10.stores.models

data class User(val id: Long, val email: String, /*this is not unique*/val username: String, val pictureLink: String)