package io.github.johnfg10.stores.models

data class IDPUser(val internalID: Long, val idpId: Long, val idpName: String, val userId: Long)