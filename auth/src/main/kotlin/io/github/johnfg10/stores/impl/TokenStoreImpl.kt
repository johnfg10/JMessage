package io.github.johnfg10.stores.impl

import io.github.johnfg10.stores.models.Token
import io.github.johnfg10.stores.TokenStore
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.jdbc.JDBCClient

class TokenStoreImpl(val vertx: Vertx, val jdbcJsonObject: JsonObject = JsonObject(), val dbClient: JDBCClient = JDBCClient.createShared(vertx, jdbcJsonObject)) : TokenStore {
    val schema = """CREATE TABLE IF NOT EXISTS tokenstore (id BIGINT UNIQUE PRIMARY KEY, userid BIGINT)"""

    init {
        dbClient.update(schema){}
    }

    override fun RemoveToken(token: Token, handler: (AsyncResult<Void>) -> Unit) {
        dbClient.updateWithParams("DELETE FROM tokenstore WHERE id = ?", JsonArray(listOf(token.tokenId))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture())
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun InsertToken(token: Token, handler: (AsyncResult<Void>) -> Unit) {
        dbClient.updateWithParams("INSERT INTO tokenstore (id, userid) VALUES (?, ?)", JsonArray(listOf(token.tokenId, token.userId))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture())
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetTokenId(id: Long, handler: (AsyncResult<Token>) -> Unit) {
        dbClient.querySingleWithParams("SELECT id, userid FROM tokenstore WHERE id = ?", JsonArray(listOf(id))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(Token(arr.getLong(0), arr.getLong(1))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetTokenUserId(id: Long, handler: (AsyncResult<Token>) -> Unit) {
        dbClient.querySingleWithParams("SELECT id, userid FROM tokenstore WHERE userid = ?", JsonArray(listOf(id))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(Token(arr.getLong(0), arr.getLong(1))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }
}