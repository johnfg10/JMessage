package io.github.johnfg10.stores.impl

import io.github.johnfg10.stores.ProfileStore
import io.github.johnfg10.stores.models.Profile
import io.github.johnfg10.stores.models.User
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.jdbc.JDBCClient

//val internalId: Long, val userId: Long, val profileInfo: JsonArray
class ProfileStoreImpl(val vertx: Vertx, val jdbcJsonObject: JsonObject = JsonObject(), val dbClient: JDBCClient = JDBCClient.createShared(vertx, jdbcJsonObject)) : ProfileStore {
    val schema = """CREATE TABLE IF NOT EXISTS profilestore (internalid BIGINT UNIQUE PRIMARY KEY, userid BIGINT UNQUE, profileinfo TEXT)"""

    init {
        dbClient.update(schema){}
    }

    override fun InsertProfile(profile: Profile, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("INSERT INTO profilestore (internalid, userid, profileinfo) VALUES (?, ?, ?)", JsonArray(listOf(profile.internalId, profile.userId, profile.profileInfo))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun DeleteProfile(profile: Profile, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("DELETE FROM profilestore WHERE internalid = ?", JsonArray(listOf(profile.internalId))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun UpdateProfile(profile: Profile, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("UPDATE profilestore SET userid = ?, profileinfo = ? WHERE internalid = ?", JsonArray(listOf(profile.userId, profile.profileInfo, profile.internalId))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetProfileInternalId(id: Long, handler: (AsyncResult<Profile>) -> Unit) {
        dbClient.querySingleWithParams("SELECT internalid, userid, profileinfo FROM profilestore WHERE internalid = ?", JsonArray(listOf(id))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(Profile(arr.getLong(0), arr.getLong(1), arr.getJsonArray(2))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetProfileUserId(id: Long, handler: (AsyncResult<Profile>) -> Unit) {
        dbClient.querySingleWithParams("SELECT internalid, userid, profileinfo FROM profilestore WHERE userid = ?", JsonArray(listOf(id))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(Profile(arr.getLong(0), arr.getLong(1), arr.getJsonArray(2))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

}