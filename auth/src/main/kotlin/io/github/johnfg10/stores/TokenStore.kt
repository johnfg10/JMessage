package io.github.johnfg10.stores

import io.github.johnfg10.stores.models.Token
import io.github.johnfg10.stores.impl.TokenStoreImpl
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

//todo add get token here
interface TokenStore {
    fun InsertToken(token: Token, handler: (AsyncResult<Void>) -> Unit)

    fun RemoveToken(token: Token, handler: (AsyncResult<Void>) -> Unit)

    fun GetTokenId(id: Long, handler: (AsyncResult<Token>) -> Unit)

    fun GetTokenUserId(id: Long, handler: (AsyncResult<Token>) -> Unit)

    companion object {
        fun Create(vertx: Vertx) : TokenStore{
            return TokenStoreImpl(vertx)
        }

        fun Create(vertx: Vertx, config: JsonObject) : TokenStore{
            return TokenStoreImpl(vertx, config)
        }
    }
}