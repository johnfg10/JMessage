package io.github.johnfg10.stores.models

data class Token(val tokenId: Long, val userId: Long)