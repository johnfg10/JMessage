package io.github.johnfg10.stores.models

import io.vertx.core.json.JsonArray

data class Profile (val internalId: Long, val userId: Long, val profileInfo: JsonArray)