package io.github.johnfg10.stores

import io.github.johnfg10.stores.impl.IDPUserStoreImpl
import io.github.johnfg10.stores.impl.TokenStoreImpl
import io.github.johnfg10.stores.models.IDPUser
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

///val internalID: Long, val idpId: Long, val idpName: String, val userName: String
//fun InsertToken(token: Token, handler: (AsyncResult<Void>) -> Unit)
interface IDPUserStore {
    fun InsertIDPUser(user: IDPUser, handler: (AsyncResult<Int>) -> Unit)

    fun DeleteIDPUser(user: IDPUser, handler: (AsyncResult<Int>) -> Unit)

    fun UpdateIDPUser(oldUser: IDPUser, newUser: IDPUser, handler: (AsyncResult<Int>) -> Unit)

    fun GetIDPUserInternalId(internalId: Long, handler: (AsyncResult<IDPUser>) -> Unit)

    fun GetIDPUserIDPId(id: Long, idpName: String, handler: (AsyncResult<IDPUser>) -> Unit)

    fun GetIDPUserIdpName(idpName: String, handler: (AsyncResult<List<IDPUser>>) -> Unit)

    fun GetIDPUserId(id: Long, handler: (AsyncResult<List<IDPUser>>) -> Unit)

    companion object {
        fun Create(vertx: Vertx) : IDPUserStore{
            return IDPUserStoreImpl(vertx)
        }

        fun Create(vertx: Vertx, config: JsonObject) : IDPUserStore{
            return IDPUserStoreImpl(vertx, config)
        }
    }
}