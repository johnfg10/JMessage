package io.github.johnfg10.stores.impl

import io.github.johnfg10.stores.models.IDPUser
import io.github.johnfg10.stores.IDPUserStore
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.jdbc.JDBCClient

///val internalID: Long, val idpId: Long, val idpName: String, val userName: String
class IDPUserStoreImpl(val vertx: Vertx, val jdbcJsonObject: JsonObject = JsonObject(), val dbClient: JDBCClient = JDBCClient.createShared(vertx, jdbcJsonObject)) : IDPUserStore {

    val schema = """CREATE TABLE IF NOT EXISTS idpuserstore (internalid BIGINT UNIQUE PRIMARY KEY, idpid BIGINT, idpname VARCHAR(100), userid BIGINT)"""

    init {
        dbClient.update(schema){}
    }

    override fun InsertIDPUser(user: IDPUser, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("INSERT INTO idpuserstore (internalid, idpid, idpname, userid) VALUES (?, ?, ?, ?)", JsonArray(listOf(user.internalID, user.idpId, user.idpName, user.userId))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun DeleteIDPUser(user: IDPUser, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("DELETE FROM idpuserstore WHERE internalid = ?", JsonArray(listOf(user.internalID))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun UpdateIDPUser(oldUser: IDPUser, newUser: IDPUser, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("UPDATE idpuserstore SET idpid = ?, idpname = ?, userid = ?  WHERE internalid = ?", JsonArray(listOf(newUser.idpId, newUser.idpName, newUser.userId, oldUser.internalID))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetIDPUserInternalId(internalId: Long, handler: (AsyncResult<IDPUser>) -> Unit) {
        dbClient.querySingleWithParams("SELECT internalid, idpid, idpname, userid FROM tokenstore WHERE internalid = ?", JsonArray(listOf(internalId))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(IDPUser(arr.getLong(0), arr.getLong(1), arr.getString(2), arr.getLong(3))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetIDPUserIDPId(id: Long, idpName: String, handler: (AsyncResult<IDPUser>) -> Unit) {
        dbClient.querySingleWithParams("SELECT internalid, idpid, idpname, userid FROM tokenstore WHERE idpid = ?, idpname = ?", JsonArray(listOf(id, idpName))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(IDPUser(arr.getLong(0), arr.getLong(1), arr.getString(2), arr.getLong(3))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetIDPUserIdpName(idpName: String, handler: (AsyncResult<List<IDPUser>>) -> Unit) {
        dbClient.queryWithParams("SELECT internalid, idpid, idpname, userid FROM tokenstore WHERE internalid = ?", JsonArray(listOf(idpName))){
            if (it.succeeded()){
                val res = it.result()
                val mutList = mutableListOf<IDPUser>()
                res.results.forEach {
                    mutList.add(IDPUser(it.getLong(0), it.getLong(1), it.getString(2), it.getLong(3)))
                }
                handler.invoke(Future.succeededFuture(mutList))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetIDPUserId(id: Long, handler: (AsyncResult<List<IDPUser>>) -> Unit) {
        dbClient.queryWithParams("SELECT internalid, idpid, idpname, userid FROM tokenstore WHERE userid = ?", JsonArray(listOf(id))){
            if (it.succeeded()){
                val res = it.result()
                val mutList = mutableListOf<IDPUser>()
                res.results.forEach {
                    mutList.add(IDPUser(it.getLong(0), it.getLong(1), it.getString(2), it.getLong(3)))
                }
                handler.invoke(Future.succeededFuture(mutList))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }
}