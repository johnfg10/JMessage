package io.github.johnfg10.stores.impl

import io.github.johnfg10.stores.models.IDPUser
import io.github.johnfg10.stores.models.User
import io.github.johnfg10.stores.UserStore
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.jdbc.JDBCClient

//val id: Long, val email: String, val username: String, val pictureLink: String
class UserStoreImpl(val vertx: Vertx, val jdbcJsonObject: JsonObject = JsonObject(), val dbClient: JDBCClient = JDBCClient.createShared(vertx, jdbcJsonObject)) : UserStore {

    val schema = """CREATE TABLE IF NOT EXISTS userstore (id BIGINT UNIQUE PRIMARY KEY, email VARCHAR(255) UNIQUE, username VARCHAR(50), picturelink VARCHAR(255))"""

    init {
        dbClient.update(schema){}
    }

    override fun InsertUser(user: User, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("INSERT INTO userstore (id, email, username, picturelink) VALUES (?, ?, ?, ?)", JsonArray(listOf(user.id, user.email, user.username, user.pictureLink))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun DeleteUser(user: User, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("DELETE FROM userstore WHERE id = ?", JsonArray(listOf(user.id))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun UpdateUser(oldUser: User, newUser: User, handler: (AsyncResult<Int>) -> Unit) {
        dbClient.updateWithParams("UPDATE userstore SET email = ?, username = ?, picturelink = ? WHERE id = ?", JsonArray(listOf(newUser.email, newUser.username, newUser.pictureLink, oldUser.id))){
            if (it.succeeded()){
                handler.invoke(Future.succeededFuture(it.result().updated))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetUserId(id: Long, handler: (AsyncResult<User>) -> Unit) {
        dbClient.querySingleWithParams("SELECT id, email, username, picturelink FROM tokenstore WHERE id = ?", JsonArray(listOf(id))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(User(arr.getLong(0), arr.getString(1), arr.getString(2), arr.getString(3))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetUserEmail(email: String, handler: (AsyncResult<User>) -> Unit) {
        dbClient.querySingleWithParams("SELECT id, email, username, picturelink FROM tokenstore WHERE email = ?", JsonArray(listOf(email))){
            if (it.succeeded()){
                val arr = it.result()
                handler.invoke(Future.succeededFuture(User(arr.getLong(0), arr.getString(1), arr.getString(2), arr.getString(3))))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }

    override fun GetUserUsername(username: String, handler: (AsyncResult<List<User>>) -> Unit) {
        dbClient.queryWithParams("SELECT id, email, username, picturelink FROM tokenstore WHERE username = ?", JsonArray(listOf(username))){
            if (it.succeeded()){
                val res = it.result()
                val mutList = mutableListOf<User>()
                res.results.forEach {
                    mutList.add(User(it.getLong(0), it.getString(1), it.getString(2), it.getString(3)))
                }
                handler.invoke(Future.succeededFuture(mutList))
            }else{
                handler.invoke(Future.failedFuture(it.cause()))
            }
        }
    }
}