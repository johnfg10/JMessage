package io.github.johnfg10.stores

import io.github.johnfg10.stores.impl.TokenStoreImpl
import io.github.johnfg10.stores.impl.UserStoreImpl
import io.github.johnfg10.stores.models.IDPUser
import io.github.johnfg10.stores.models.User
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

//val id: Long, val email: String, val username: String, val pictureLink: String
interface UserStore {
    fun InsertUser(user: User, handler: (AsyncResult<Int>) -> Unit)

    fun DeleteUser(user: User, handler: (AsyncResult<Int>) -> Unit)

    fun UpdateUser(oldUser: User, newUser: User, handler: (AsyncResult<Int>) -> Unit)

    fun GetUserId(id: Long, handler: (AsyncResult<User>) -> Unit)

    fun GetUserEmail(email: String, handler: (AsyncResult<User>) -> Unit)

    fun GetUserUsername(username: String, handler: (AsyncResult<List<User>>) -> Unit)

    companion object {
        fun Create(vertx: Vertx) : UserStore{
            return UserStoreImpl(vertx)
        }

        fun Create(vertx: Vertx, config: JsonObject) : UserStore{
            return UserStoreImpl(vertx, config)
        }
    }
}