package io.github.johnfg10.providers

import io.github.johnfg10.AuthHandlerProvider
import io.github.johnfg10.stores.models.IDPUser
import io.github.johnfg10.stores.models.User
import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.oauth2.AccessToken
import io.vertx.ext.auth.oauth2.providers.GoogleAuth
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.OAuth2AuthHandler
import javax.jws.soap.SOAPBinding

class GoogleAuthProvider(val cliendId: String, val clientSecret: String,
                         val vertx: Vertx, val router: Router,
                         val scopes: Set<String> = setOf("https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/plus.login", "https://www.googleapis.com/auth/userinfo.email") ,
                         val callbackUrl: String = "http://localhost:8080/google/callback") : AuthHandlerProvider {

    override fun GetAuthName(): String {
        return "google"
    }

    val authProvider = GoogleAuth.create(vertx, cliendId, clientSecret)
    val authHandler = OAuth2AuthHandler.create(authProvider, callbackUrl)

    init {

    }

    override fun HandleAuth(routingCtx: RoutingContext) {
        authHandler.addAuthorities(scopes)
        authHandler.setupCallback(router.route())
    }

    override fun GetAuthProviderButton(): String {
        return ""
    }

    override fun GetAccessToken(code: String, handler: (AsyncResult<AccessToken>) -> Unit)
    {
        authProvider.decodeToken(code){
            handler.invoke(it)
        }
    }
}