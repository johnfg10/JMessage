package io.github.johnfg10

import io.github.johnfg10.stores.models.IDPUser
import io.github.johnfg10.stores.models.User
import io.vertx.core.AsyncResult
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.oauth2.AccessToken
import io.vertx.ext.web.RoutingContext

interface AuthHandlerProvider {
    fun HandleAuth(routingCtx: RoutingContext)

    /**
     * ^LINK^ will be replaced with the required link
     * example <a href="^LINK^">google</a>
     */
    fun GetAuthProviderButton() : String

    fun GetAccessToken(code: String,handler: (AsyncResult<AccessToken>) -> Unit)

    fun GetAuthName() : String
}